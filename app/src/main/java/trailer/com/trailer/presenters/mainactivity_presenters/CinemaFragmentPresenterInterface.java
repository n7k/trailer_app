package trailer.com.trailer.presenters.mainactivity_presenters;

import java.util.List;

import trailer.com.trailer.database.CinemaDbModel;

public interface CinemaFragmentPresenterInterface {

   List<CinemaDbModel> getMainActivityData(int key);

}
