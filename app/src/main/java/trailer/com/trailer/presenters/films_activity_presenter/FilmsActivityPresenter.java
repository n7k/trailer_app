package trailer.com.trailer.presenters.films_activity_presenter;


import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import trailer.com.trailer.database.DBManipulator;
import trailer.com.trailer.database.FilmsDbModel;
import trailer.com.trailer.database.FilmsDescriptionDbModel;
import trailer.com.trailer.database.SessionsDBModel;

public class FilmsActivityPresenter implements FilmsActivityPresenterActivity{

    public final String BODY = "https://kinoafisha.ua";

    public FilmsActivityPresenter() {
    }

    public String getFilmName(String key){
        return DBManipulator.getInstance().getData(FilmsDbModel.class, "image", key).get(0).getName();
    }

    public String getFilmCountry(String key){
        return DBManipulator.getInstance().getData(FilmsDbModel.class, "image", key).get(0).getCountries();
    }

    public String getFilmRejisser(String key){
        return DBManipulator.getInstance().getData(FilmsDbModel.class, "image", key).get(0).getRejisser();
    }

    public String getFilmActors(String key){
        return DBManipulator.getInstance().getData(FilmsDbModel.class, "image", key).get(0).getActors();
    }

    public String getFilmVote(String key){
        return DBManipulator.getInstance().getData(FilmsDbModel.class, "image", key).get(0).getVote();
    }

    public String getFilmDescription(String filmName){
        return DBManipulator.getInstance().getData(FilmsDescriptionDbModel.class, "title", filmName).get(0).getDescription();
    }

    public void setPicture(Context context, String url, ImageView imageView){
        Glide.with(context).load(BODY+url).into(imageView);
    }

    public List<SessionsDBModel> sessionsExtractor(List<FilmsDbModel> film){
        List<SessionsDBModel> sessionsDBModelList = new ArrayList<>();
        for(FilmsDbModel model : film){
            sessionsDBModelList=model.getSessionsList();
        }
        return sessionsDBModelList;
    }

    public List<String> countryAndJanrSeparator(String countryJanr){
        String[] parts = countryJanr.split("\\(");
        String country = parts[0].trim();
        String janr = parts[1].substring(0, parts[1].length()-1);
        List<String> countryAndJanrList = new ArrayList<>();
        countryAndJanrList.add(country);
        countryAndJanrList.add(janr);
        return countryAndJanrList;
    }

    public String htmlToStringParser(String htmlSample){
        String endResult = "";
        if(!htmlSample.equalsIgnoreCase("")) {
            String[] parts = htmlSample.split(",");
            for (int i = 0; i < parts.length; i++) {
                String[] tempSubString = parts[i].split("\">");
                if (i == (parts.length - 1)) {
                    try {
                        endResult += tempSubString[1].substring(0, tempSubString[1].length() - 4) + ";" + "\n";
                    } catch (IndexOutOfBoundsException e) {
                        if (!tempSubString[0].startsWith(" ")) {
                            endResult += tempSubString[0] + "," + "\n";
                        }else {
                            endResult += tempSubString[0].substring(1, tempSubString[0].length()) + ";";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else
                    try {
                        endResult += tempSubString[1].substring(0, tempSubString[1].length() - 4) + "," + "\n";
                    } catch (IndexOutOfBoundsException e) {
                        if (!tempSubString[0].startsWith(" ")) {
                            endResult += tempSubString[0] + "," + "\n";
                        }else {
                            endResult += tempSubString[0].substring(1, tempSubString[0].length()) + "," + "\n";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        }
        return endResult;
    }

}
