package trailer.com.trailer.presenters.startactivity_presenter;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import trailer.com.trailer.database.CinemaDbModel;
import trailer.com.trailer.database.CityDbModel;
import trailer.com.trailer.database.DBManipulator;
import trailer.com.trailer.database.FilmsDbModel;
import trailer.com.trailer.database.FilmsDescriptionDbModel;
import trailer.com.trailer.database.SessionsDBModel;
import trailer.com.trailer.internet.Api;
import trailer.com.trailer.internet.RetrofitProvider;
import trailer.com.trailer.internet.city_and_cinema_model.CinemasAndCitiesModel;
import trailer.com.trailer.mappers.CinemaMapper;
import trailer.com.trailer.mappers.CitiesMapper;

public class StartActivityPresenter implements StartActivityPresenterInterface {

    private CitiesMapper citiesMapper;
    private CinemaMapper cinemaMapper;
    private StartActivityView view;
    boolean isConnected;

    public StartActivityPresenter(StartActivityView view, boolean isConnected) {
        this.view = view;
        this.isConnected = isConnected;
    }

    @Override
    public void generateApiCall(boolean isConnected) {
        if (!DBManipulator.getInstance().hasData(CityDbModel.class)) {
            RetrofitProvider retrofitProvider = RetrofitProvider.getInstance();
            Api api = retrofitProvider.getApi();
            api.getCinemasAndCities().enqueue(new Callback<CinemasAndCitiesModel>() {
                @Override
                public void onResponse(Call<CinemasAndCitiesModel> call, Response<CinemasAndCitiesModel> response) {
                    if (response.isSuccessful()) {
                        citiesMapper = new CitiesMapper();
                        cinemaMapper = new CinemaMapper();
                        DBManipulator.getInstance().saveData(citiesMapper.map(response.body().getCities()));
                        DBManipulator.getInstance().saveData(cinemaMapper.map(response.body().getCinemas()));
                        view.onDataLoaded(getStartActivityNameData());
                    } else {
                        view.onDataLoadingFailed();
                        Log.e("Response Error", String.valueOf(response.errorBody()));
                    }
                }

                @Override
                public void onFailure(Call<CinemasAndCitiesModel> call, Throwable t) {
                    view.onDataLoadingFailed();
                    Log.e("TAG", t.getMessage());
                }
            });
        } else if (System.currentTimeMillis() >= DBManipulator.getInstance().getData(CityDbModel.class).get(0).getUpdateTime() + 43200000 && isConnected) {

            DBManipulator.getInstance().deleteData();
            Log.d("Test", "Data has updated!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            RetrofitProvider retrofitProvider = RetrofitProvider.getInstance();
            Api api = retrofitProvider.getApi();
            api.getCinemasAndCities().enqueue(new Callback<CinemasAndCitiesModel>() {
                @Override
                public void onResponse(Call<CinemasAndCitiesModel> call, Response<CinemasAndCitiesModel> response) {
                    if (response.isSuccessful()) {
                        citiesMapper = new CitiesMapper();
                        cinemaMapper = new CinemaMapper();
                        DBManipulator.getInstance().saveData(citiesMapper.map(response.body().getCities()));
                        DBManipulator.getInstance().saveData(cinemaMapper.map(response.body().getCinemas()));
                        view.onDataLoaded(getStartActivityNameData());

                    } else {
                        view.onDataLoadingFailed();
                        Log.e("Response Error", String.valueOf(response.errorBody()));
                    }
                }

                @Override
                public void onFailure(Call<CinemasAndCitiesModel> call, Throwable t) {
                    view.onDataLoadingFailed();
                    Log.e("TAG", t.getMessage());
                }
            });

        } else {
            view.onDataLoaded(getStartActivityNameData());
        }
    }

    @Override
    public List<String> getStartActivityNameData() {
        List<String> cityList = new ArrayList<>();
        List<CityDbModel> cities = DBManipulator.getInstance().getData(CityDbModel.class);
        for (int i = 0; i < cities.size(); i++) {
            cityList.add(cities.get(i).getCityName());
        }
        return cityList;
    }

    @Override
    public int getStartActivityIdData(int position) {
        List<CityDbModel> cities = DBManipulator.getInstance().getData(CityDbModel.class);
        return cities.get(position).getCityId();
    }

    @Override
    public int onItemClick(View view, int position) {
        return getStartActivityIdData(position);
    }

    public interface StartActivityView {

        void onDataLoaded(List<String> cities);

        void onDataLoadingFailed();
    }


}