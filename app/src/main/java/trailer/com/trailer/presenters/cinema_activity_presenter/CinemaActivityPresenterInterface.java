package trailer.com.trailer.presenters.cinema_activity_presenter;


import java.util.List;

import trailer.com.trailer.database.FilmsDbModel;

public interface CinemaActivityPresenterInterface {

    String getCinemaName(int id);
    String getCinemaAddress(int id);
    String getCinemaPhone(int id);
    List<String> getCinemaFilmsOnly(List<FilmsDbModel> filmList);
}
