package trailer.com.trailer.presenters.mainactivity_presenters;


import android.util.Log;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import trailer.com.trailer.database.DBManipulator;
import trailer.com.trailer.database.FilmsDbModel;
import trailer.com.trailer.internet.films_and_sessions_model.FilmsAndSessionsModel;
import trailer.com.trailer.internet.films_description_model.FilmsModel;
import trailer.com.trailer.internet.Api;
import trailer.com.trailer.internet.RetrofitProvider;
import trailer.com.trailer.mappers.FilmsDescriptionMapper;
import trailer.com.trailer.mappers.FilmsMapper;

public class FilmsFragmentPresenter implements FilmsFragmentPresenterInterface {

    FilmsMapper filmsMapper;
    LoadCallBack loadCallBack;
    FilmsDescriptionMapper filmsDescriptionMapper;
    FilmsModel filmsModel;

    public FilmsFragmentPresenter(LoadCallBack loadCallBack){
        this.loadCallBack = loadCallBack;
    }


    @Override
    public void getAllFilmsData() {
        if (!DBManipulator.getInstance().hasData(FilmsDbModel.class)) {
            RetrofitProvider provider = RetrofitProvider.getInstance();
            Api api = provider.getApi();
            api.getFilms().enqueue(new Callback<List<FilmsModel>>() {
                @Override
                public void onResponse(Call<List<FilmsModel>> call, Response<List<FilmsModel>> response) {
                    if (response.isSuccessful()) {
                        filmsDescriptionMapper = new FilmsDescriptionMapper();
                        filmsModel = new FilmsModel();
                        DBManipulator.getInstance().saveData(filmsDescriptionMapper.map(filmsModel.FilmsDescriptionAsList(response.body())));
                        loadCallBack.onDataLoaded();
                    } else {
                        Log.e("Response Error", response.errorBody().toString());
                        loadCallBack.onDataFailed();
                    }
                }

                @Override
                public void onFailure(Call<List<FilmsModel>> call, Throwable t) {

                }
            });
            api.getFilmsAndSessions().enqueue(new Callback<FilmsAndSessionsModel>() {
                @Override
                public void onResponse(Call<FilmsAndSessionsModel> call, Response<FilmsAndSessionsModel> response) {
                    if (response.isSuccessful()) {
                        filmsMapper = new FilmsMapper();
                        DBManipulator.getInstance().saveData(filmsMapper.map(response.body().getFilms()));
                        loadCallBack.onDataLoaded();
                    } else {
                        Log.e("Response Error", response.errorBody().toString());
                        loadCallBack.onDataFailed();
                    }
                }
                @Override
                public void onFailure(Call<FilmsAndSessionsModel> call, Throwable t) {
                    Log.e("Tag", t.getMessage().toString());
                    loadCallBack.onDataFailed();
                }
            });
        }
        else {
            loadCallBack.onDataLoaded();
        }
    }
    public interface LoadCallBack{
        void onDataLoaded();
        void onDataFailed();
    }
}
