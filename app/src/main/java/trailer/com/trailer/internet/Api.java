package trailer.com.trailer.internet;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import trailer.com.trailer.internet.films_description_model.FilmsModel;
import trailer.com.trailer.internet.city_and_cinema_model.CinemasAndCitiesModel;
import trailer.com.trailer.internet.films_and_sessions_model.FilmsAndSessionsModel;

public interface Api {

    @GET("/cinemaList.json")
    Call<CinemasAndCitiesModel> getCinemasAndCities();

    @GET("/ajax/kinoafisha_load")
    Call<FilmsAndSessionsModel> getFilmsAndSessions();

    @GET("/movieList.json")
    Call<List<FilmsModel>> getFilms();
}