package trailer.com.trailer.internet.films_description_model;



import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class FilmsModel {

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("images")
    private List<String> images;

    @SerializedName("trailers")
    private List<String> trailers;


    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getImages() {
        return images;
    }

    public List<String> getTrailers() {
        return trailers;
    }

    public List<FilmsDescription> FilmsDescriptionAsList(List<FilmsModel> value){
        List<FilmsDescription> filmsDescriptions = new ArrayList<>();
        for(FilmsModel model : value){
            FilmsDescription description = new FilmsDescription();
            description.setTitle(model.getTitle());
            description.setDescription(model.getDescription());
            description.setImages(model.getImages());
            description.setTrailers(model.getTrailers());
            filmsDescriptions.add(description);
        }
        return filmsDescriptions;
    }

    @Override
    public String toString() {
        return "FilmsModel{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", images=" + images +
                ", trailers=" + trailers +
                '}';
    }
}
