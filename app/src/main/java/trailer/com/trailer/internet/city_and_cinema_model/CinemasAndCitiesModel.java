package trailer.com.trailer.internet.city_and_cinema_model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CinemasAndCitiesModel {

    @SerializedName("cities")
    private List<List<String>> cities;

    @SerializedName("cinemas")
    private List<List<String>> cinemas;

    public List<City> getCities() {
        List<City> cityList = new ArrayList<>();
        for (List<String> value : cities) {
            City city = new City(Integer.valueOf(value.get(0)), value.get(1));
            cityList.add(city);
        }
        return cityList;
    }

    public List<Cinema> getCinemas() {
        List<Cinema> cinemaList = new ArrayList<>();
        for (List<String> value : cinemas) {
            Cinema cinema = new Cinema(Integer.valueOf(value.get(0)), Integer.valueOf(value.get(1)), value.get(2), value.get(3), value.get(4));
            cinemaList.add(cinema);
        }
        return cinemaList;
    }

    @Override
    public String toString() {
        return "Cities: " + cities.toString() + "\n" + "Cinemas: " + cinemas.toString();
    }
}