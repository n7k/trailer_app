package trailer.com.trailer.internet.city_and_cinema_model;

public class Cinema {

    private int cityId;
    private int cinemaId;
    private String cinemaName;
    private String address;
    private String phoneNumber;

    public void setCinemaId(int cinemaId) {
        this.cinemaId = cinemaId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Cinema(){
        cinemaId = 0;
        cityId = 0;

        cinemaName = "";
        address = "";
        phoneNumber = "";
    }

    public Cinema(int cinemaId, int cityId, String  cinemaName, String address, String phoneNumber){
        this.cinemaId = cinemaId;
        this.cinemaName = cinemaName;
        this.cityId = cityId;
        this.address = address;
        this.phoneNumber = phoneNumber;
    }

    public int getCinemaId() {
        return cinemaId;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getCityId() {
        return cityId;
    }

    public String getCinemaName() {
        return cinemaName;
    }

    public String toString(){
        return "{" + "ID " + cinemaId + " City " + cityId + " Cinema name: " + cinemaName + " Cinema Address " + address + " phoneNumber " + phoneNumber +  "}";
    }

}