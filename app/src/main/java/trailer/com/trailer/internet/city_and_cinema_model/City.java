package trailer.com.trailer.internet.city_and_cinema_model;

public class City {

    private int id;
    private String name;

    public City(){
        id = 0;
        name = "";
    }

    public City(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId(){
        return id;
    }

    public String getName(){
        return name;
    }


    @Override
    public String toString() {
        return "ID: " + id + ", name = " + name;
    }
}