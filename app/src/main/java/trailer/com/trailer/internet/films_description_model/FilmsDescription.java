package trailer.com.trailer.internet.films_description_model;

import java.util.ArrayList;
import java.util.List;

public class FilmsDescription {
    private String title;
    private String description;
    private List<String> images;
    private List<String> trailers;


    public FilmsDescription(String title, String description, List<String> images, List<String> trailers) {
        this.title = title;
        this.description = description;
        this.images = images;
        this.trailers = trailers;
    }

    public FilmsDescription() {
        title = "";
        description = "";
        images = new ArrayList<>();
        trailers = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public List<String> getTrailers() {
        return trailers;
    }

    public void setTrailers(List<String> trailers) {
        this.trailers = trailers;
    }


    @Override
    public String toString() {
        return "FilmsDescription{" +
                "title='" + title + '\'' +
                ", description=" + description +
                ", images=" + images +
                ", trailers=" + trailers +
                '}';
    }
}
