package trailer.com.trailer.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import trailer.com.trailer.R;
import trailer.com.trailer.database.DBManipulator;
import trailer.com.trailer.database.FilmsDbModel;
import trailer.com.trailer.presenters.mainactivity_presenters.FilmsFragmentPresenter;
import trailer.com.trailer.ui.FilmsActivity;
import trailer.com.trailer.ui.adapters.FilmsRecyclerViewAdapter;
import trailer.com.trailer.ui.listener.RecyclerItemClickListener;


public class FilmsFragment extends Fragment implements FilmsFragmentPresenter.LoadCallBack {

    private final static int LAYOUT = R.layout.films_fragment_layout;

    private View view;
    private RecyclerView filmsRecyclerView;
    FilmsRecyclerViewAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(LAYOUT, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        filmsRecyclerView = view.findViewById(R.id.filmsRecyclerView);
        filmsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new FilmsRecyclerViewAdapter(getContext());
        filmsRecyclerView.setAdapter(adapter);
        filmsRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), filmsRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Intent intent = new Intent(getContext(), FilmsActivity.class);
                intent.putExtra("FilmImage", DBManipulator.getInstance().getData(FilmsDbModel.class).get(position).getImage());
                startActivity(intent);
            }

            @Override
            public void onLongItemClick(View v, int position) {

            }
        }));
        final FilmsFragmentPresenter filmsFragmentPresenter = new FilmsFragmentPresenter(this);
        filmsFragmentPresenter.getAllFilmsData();
    }
    @Override
    public void onDataLoaded() {
        adapter.addData(DBManipulator.getInstance().getData(FilmsDbModel.class));
    }

    @Override
    public void onDataFailed() {

    }
}
