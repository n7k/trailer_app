package trailer.com.trailer.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import trailer.com.trailer.R;
import trailer.com.trailer.presenters.mainactivity_presenters.CinemaFragmentPresenter;
import trailer.com.trailer.presenters.mainactivity_presenters.FilmsFragmentPresenter;
import trailer.com.trailer.ui.adapters.TabsPagerFragmentAdapter;
import trailer.com.trailer.ui.fragments.CinemasFragment;
import trailer.com.trailer.ui.fragments.FilmsFragment;

public class MainActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tabLayout;
    TabsPagerFragmentAdapter adapter;
    int intentMassage;
    CinemasFragment cinemasFragment;
    FilmsFragment filmsFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        intentMassage = intent.getIntExtra("cityId", 0);
        cinemasFragment = new CinemasFragment();
        filmsFragment = new FilmsFragment();
        cinemasFragment.getIntentMassage(intentMassage);
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.containerForCinemas);
        adapter = new TabsPagerFragmentAdapter(getSupportFragmentManager());
        adapter.addFragment(cinemasFragment, "Кинотеатры");
        adapter.addFragment(filmsFragment, "Фильмы");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    protected void onResume(){
        super.onResume();

    }
    protected void onDestroy() {
        super.onDestroy();
    }

}