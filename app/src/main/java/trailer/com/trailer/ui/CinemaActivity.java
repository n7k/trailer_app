package trailer.com.trailer.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import trailer.com.trailer.R;
import trailer.com.trailer.database.DBManipulator;
import trailer.com.trailer.database.FilmsDbModel;
import trailer.com.trailer.presenters.cinema_activity_presenter.CinemaActivityPresenter;
import trailer.com.trailer.presenters.cinema_activity_presenter.MapPresenter;
import trailer.com.trailer.ui.adapters.CinemaActivityRecyclerViewAdapter;
import trailer.com.trailer.ui.listener.RecyclerItemClickListener;

public class CinemaActivity extends FragmentActivity implements OnMapReadyCallback{

    @BindView(R.id.cinemaName) TextView cinemaName;
    @BindView(R.id.cinemaAddress) TextView cinemaAddress;
    @BindView(R.id.cinemaContacts) TextView cinemaContacts;
    @BindView(R.id.emptyRecycler) TextView emptyRecycler;
    CinemaActivityPresenter presenter;
    int intentMassage;
    MapPresenter mapPresenter;
    private GoogleMap mMap;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager manager;
    CinemaActivityRecyclerViewAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cinema_activity_layout);
        ButterKnife.bind(this);
        final Intent intent = getIntent();
        intentMassage = intent.getIntExtra("id", 0);
        presenter = new CinemaActivityPresenter(intentMassage);
        MapFragment mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapPresenter = new MapPresenter(this);
        recyclerView = findViewById(R.id.filmsInCinemaRecyclerView);
        manager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(manager);
        adapter = new CinemaActivityRecyclerViewAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Intent filmIntent = new Intent(getApplicationContext(), FilmsActivity.class);
                filmIntent.putExtra("FilmImage", presenter.getCinemaFilmsOnly(DBManipulator.getInstance().getData(FilmsDbModel.class)).get(position));
                startActivity(filmIntent);
            }

            @Override
            public void onLongItemClick(View v, int position) {

            }
        }));
    }
    protected void onResume(){
        super.onResume();
        cinemaName.setText(presenter.getCinemaName(intentMassage));
        cinemaAddress.setText("Aдресс: " + "\n" + presenter.getCinemaAddress(intentMassage));
        cinemaContacts.setText("Телефон: " + "\n" + presenter.getCinemaPhone(intentMassage));
        adapter.addData(presenter.getCinemaFilmsOnly(DBManipulator.getInstance().getData(FilmsDbModel.class)));
        if (adapter.adapterIsEmpty()){
            recyclerView.setVisibility(View.GONE);
            emptyRecycler.setVisibility(View.VISIBLE);
        }else {
            emptyRecycler.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mapPresenter.centerMapOnLocation(mapPresenter.getPosition(presenter.getCinemaAddress(intentMassage)), mMap, presenter.getCinemaName(intentMassage));
    }
}
