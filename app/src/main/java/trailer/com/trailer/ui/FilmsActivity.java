package trailer.com.trailer.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import trailer.com.trailer.R;
import trailer.com.trailer.database.DBManipulator;
import trailer.com.trailer.database.FilmsDbModel;
import trailer.com.trailer.presenters.films_activity_presenter.FilmsActivityPresenter;
import trailer.com.trailer.ui.adapters.FilmsActivitySessionsAdapter;
import trailer.com.trailer.ui.listener.RecyclerItemClickListener;


public class FilmsActivity extends AppCompatActivity {

    @BindView(R.id.filmTitle)TextView filmTitle;
    @BindView(R.id.filmPoster)ImageView filmPoster;
    @BindView(R.id.filmCountry)TextView filmCountry;
    @BindView(R.id.filmJanr)TextView filmJanr;
    @BindView(R.id.filmRejisser)TextView filmRejisser;
    @BindView(R.id.filmActors) TextView filmActors;
    @BindView(R.id.filmVote) TextView filmVote;
    @BindView(R.id.filmDescription) TextView filmDescription;
    @BindView(R.id.filmSessionsRecyclerView)
    RecyclerView filmSessionsRecycler;

    FilmsActivityPresenter filmsActivityPresenter;
    String imageUrl;
    FilmsActivitySessionsAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.films_activity_layout);
        ButterKnife.bind(this);

        Intent intentFromCinema = getIntent();
        imageUrl = intentFromCinema.getStringExtra("FilmImage");

        filmsActivityPresenter = new FilmsActivityPresenter();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        filmSessionsRecycler.setLayoutManager(layoutManager);
        adapter = new FilmsActivitySessionsAdapter(this);
        filmSessionsRecycler.setAdapter(adapter);
        filmSessionsRecycler.addOnItemTouchListener(new RecyclerItemClickListener(this, filmSessionsRecycler, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {

            }

            @Override
            public void onLongItemClick(View v, int position) {

            }
        }));

    }

    public void onResume(){
        super.onResume();
        filmTitle.setText(filmsActivityPresenter.getFilmName(imageUrl));
        filmCountry.setText(filmsActivityPresenter.countryAndJanrSeparator(filmsActivityPresenter.getFilmCountry(imageUrl)).get(0));
        filmJanr.setText(filmsActivityPresenter.countryAndJanrSeparator(filmsActivityPresenter.getFilmCountry(imageUrl)).get(1));
        filmActors.setText("Актеры: \n" + filmsActivityPresenter.htmlToStringParser(filmsActivityPresenter.getFilmActors(imageUrl)));
        filmRejisser.setText("Режиссеры: \n" + filmsActivityPresenter.htmlToStringParser(filmsActivityPresenter.getFilmRejisser(imageUrl)));
        filmsActivityPresenter.setPicture(this, imageUrl, filmPoster);
        filmVote.setText("Оценка: " + filmsActivityPresenter.getFilmVote(imageUrl));
        filmDescription.setText(Html.fromHtml(filmsActivityPresenter.getFilmDescription(filmsActivityPresenter.getFilmName(imageUrl))));
        adapter.addData(filmsActivityPresenter.sessionsExtractor(DBManipulator.getInstance().getData(FilmsDbModel.class, "image", imageUrl)));
    }
}
