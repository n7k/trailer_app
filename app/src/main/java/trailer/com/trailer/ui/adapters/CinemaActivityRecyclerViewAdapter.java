package trailer.com.trailer.ui.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import trailer.com.trailer.R;

public class CinemaActivityRecyclerViewAdapter extends RecyclerView.Adapter<CinemaActivityRecyclerViewAdapter.CinemaLayoutViewHolder> {

    LayoutInflater inflater;
    List<String> imageList = new ArrayList<>();
    Context context;
    private final static String BODY = "https://kinoafisha.ua/";

    public class CinemaLayoutViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        public CinemaLayoutViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.filmsImage);
        }
    }

    public CinemaActivityRecyclerViewAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public CinemaLayoutViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.images_recycler_view_row, parent, false);
        CinemaLayoutViewHolder holder = new CinemaLayoutViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CinemaLayoutViewHolder holder, int position) {
        Glide.with(context).load(BODY + imageList.get(position)).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    public void addData(List<String> imageList) {
        this.imageList = imageList;
        notifyDataSetChanged();
    }

    public boolean adapterIsEmpty(){
        return imageList.isEmpty();
    }
}
