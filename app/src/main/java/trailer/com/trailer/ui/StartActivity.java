package trailer.com.trailer.ui;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.List;

import trailer.com.trailer.R;
import trailer.com.trailer.database.DBManipulator;
import trailer.com.trailer.presenters.startactivity_presenter.StartActivityPresenter;
import trailer.com.trailer.ui.adapters.CityRecyclerViewAdapter;
import trailer.com.trailer.ui.listener.RecyclerItemClickListener;

public class StartActivity extends FragmentActivity implements StartActivityPresenter.StartActivityView {

    RecyclerView recyclerView;
    CityRecyclerViewAdapter adapter;
    StartActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_activity_layout);
        presenter = new StartActivityPresenter(this, isOnline());
        recyclerView = findViewById(R.id.recycleContainer);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CityRecyclerViewAdapter(getApplicationContext());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, new RecyclerItemClickListener.OnItemClickListener(){
            @Override
            public void onItemClick(View v, int position) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("cityId",  presenter.onItemClick(v, position));
                startActivity(intent);
            }

            @Override
            public void onLongItemClick(View v, int position) {
                Log.d("Tag", "Nothing Happening");
            }
        }));
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.generateApiCall(isOnline());
    }

    @Override
    public void onDataLoaded(List<String> cities) {
        adapter.addData(cities);
    }

    @Override
    public void onDataLoadingFailed() {
        //TODO show error
    }

    protected void onDestroy(){
        DBManipulator.getInstance().onRealmClose();
        super.onDestroy();
    }
    public boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }
}