package trailer.com.trailer.ui.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import trailer.com.trailer.R;
import trailer.com.trailer.database.FilmsDbModel;

public class FilmsRecyclerViewAdapter extends RecyclerView.Adapter<FilmsRecyclerViewAdapter.MyViewHolder> {


    LayoutInflater inflater;
    List<FilmsDbModel> filmList = new ArrayList<>();
    Context context;
    static final String BODY = "https://kinoafisha.ua";

    public class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView posterImageView;
        TextView name;
        TextView country;
        TextView janr;
        TextView rejisser;
        TextView actors;
        TextView vote;

        public MyViewHolder(View itemView) {
            super(itemView);
            posterImageView = itemView.findViewById(R.id.posterImageView);
            name = itemView.findViewById(R.id.name);
            country = itemView.findViewById(R.id.country);
            janr = itemView.findViewById(R.id.janr);
            rejisser = itemView.findViewById(R.id.rejisser);
            actors = itemView.findViewById(R.id.actors);
            vote = itemView.findViewById(R.id.vote);
        }
    }

    public FilmsRecyclerViewAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.films_recyclerview_row, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(v);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.name.setText(filmList.get(position).getName());
        holder.country.setText(countryAndJanrSeparator(filmList.get(position).getCountries()).get(0));
        holder.janr.setText(countryAndJanrSeparator(filmList.get(position).getCountries()).get(1));
        holder.rejisser.setText("Режиссеры: \n" + htmlToStringParser(filmList.get(position).getRejisser()));
        holder.actors.setText(htmlToStringParser(filmList.get(position).getActors()));
        holder.vote.setText("Оценка: " + filmList.get(position).getVote());
        Glide.with(context).load(BODY+filmList.get(position).getImage()).into(holder.posterImageView);
    }

    @Override
    public int getItemCount() {
        return filmList.size();
    }

    public void addData(List<FilmsDbModel> films){
       filmList = films;
       notifyDataSetChanged();
    }

    public List<String> countryAndJanrSeparator(String countryJanr){
        String[] parts = countryJanr.split("\\(");
        String country = parts[0].trim();
        String janr = parts[1].substring(0, parts[1].length()-1);
        List<String> countryAndJanrList = new ArrayList<>();
        countryAndJanrList.add(country);
        countryAndJanrList.add(janr);
        return countryAndJanrList;
    }

    public String htmlToStringParser(String htmlSample){
        String endResult = "";
        if(!htmlSample.equalsIgnoreCase("")) {
            String[] parts = htmlSample.split(",");
            for (int i = 0; i < parts.length; i++) {
                String[] tempSubString = parts[i].split("\">");
                if (i == (parts.length - 1)) {
                    try {
                        endResult += tempSubString[1].substring(0, tempSubString[1].length() - 4) + ";" + "\n";
                    } catch (IndexOutOfBoundsException e) {
                        if (!tempSubString[0].startsWith(" ")) {
                            endResult += tempSubString[0] + "," + "\n";
                        }else {
                            endResult += tempSubString[0].substring(1, tempSubString[0].length()) + ";";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else
                    try {
                        endResult += tempSubString[1].substring(0, tempSubString[1].length() - 4) + "," + "\n";
                    } catch (IndexOutOfBoundsException e) {
                        if (!tempSubString[0].startsWith(" ")) {
                            endResult += tempSubString[0] + "," + "\n";
                        }else {
                            endResult += tempSubString[0].substring(1, tempSubString[0].length()) + "," + "\n";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            }
        }
        return endResult;
    }

}
