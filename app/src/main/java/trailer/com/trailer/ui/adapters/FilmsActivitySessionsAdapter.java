package trailer.com.trailer.ui.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import trailer.com.trailer.R;
import trailer.com.trailer.database.SessionsDBModel;

public class FilmsActivitySessionsAdapter extends RecyclerView.Adapter<FilmsActivitySessionsAdapter.SessionsViewHolder>{



    List<SessionsDBModel> sessions = new ArrayList<>();

    LayoutInflater inflater;

    int counter = 0;

    public FilmsActivitySessionsAdapter(Context context) {
        inflater = LayoutInflater.from(context);
    }

    class SessionsViewHolder extends RecyclerView.ViewHolder{

        TextView cinemaName;
        TextView sessionTime;

        public SessionsViewHolder(View itemView) {
            super(itemView);
            cinemaName = itemView.findViewById(R.id.cinemaNameSession);
            sessionTime = itemView.findViewById(R.id.sessionsTime);
        }
    }

    @Override
    public SessionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.film_sessions_recycler_row, parent, false);
        SessionsViewHolder viewHolder = new SessionsViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SessionsViewHolder holder, int position) {
        counter++;
        if (sessions.get(position).getCinemaName() != null) {
            holder.cinemaName.setText(sessions.get(position).getCinemaName() + "\n" + sessions.get(position).getHallName() + "\n" + sessions.get(position).getCinemaAddress());
        } else if (sessions.get(position).getCinemaName() == null) {
                int tempCounter = counter;
                do {
                    --tempCounter;
                } while (sessions.get(tempCounter).getCinemaName()== null);
                holder.cinemaName.setText(sessions.get(tempCounter).getCinemaName() + "\n" + sessions.get(position).getHallName());
            }
        holder.sessionTime.setText(Html.fromHtml(sessions.get(position).getSessionsFromSessions().toString()));
    }
    @Override
    public int getItemCount() {
        return sessions.size();
    }

    public void addData(List<SessionsDBModel> sessions){
        this.sessions = sessions;
        notifyDataSetChanged();
    }
}
