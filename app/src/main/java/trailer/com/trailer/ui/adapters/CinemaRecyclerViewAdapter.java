package trailer.com.trailer.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import trailer.com.trailer.R;
import trailer.com.trailer.database.CinemaDbModel;
import trailer.com.trailer.presenters.mainactivity_presenters.CinemaFragmentPresenter;


public class CinemaRecyclerViewAdapter extends RecyclerView.Adapter<CinemaRecyclerViewAdapter.MyViewHolder> {

    LayoutInflater inflater;
    List<CinemaDbModel> cinemasDataSet = new ArrayList<>();
    Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView cinemaName;
        TextView cinemaAddress;
        TextView cinemaPhone;


        public MyViewHolder(View itemView) {
            super(itemView);
            cinemaName = itemView.findViewById(R.id.cinemaName);
            cinemaAddress = itemView.findViewById(R.id.cinemaAddress);
            cinemaPhone = itemView.findViewById(R.id.cinemaPhoneNumber);
        }
    }

    public CinemaRecyclerViewAdapter(Context context){
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.cinema_recyclerview_row, parent, false);
        MyViewHolder holder = new MyViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.cinemaName.setText(cinemasDataSet.get(position).getCinemaName());
        holder.cinemaAddress.setText(cinemasDataSet.get(position).getAddress());
        holder.cinemaPhone.setText(cinemasDataSet.get(position).getPhoneNumber());
    }

    @Override
    public int getItemCount() {
        return cinemasDataSet.size();
    }

    public void addData(List<CinemaDbModel> cinemas){
        cinemasDataSet = cinemas;
        notifyDataSetChanged();
    }

}
