package trailer.com.trailer.database;

import io.realm.RealmObject;


public class SessionsDBModel extends RealmObject {

    private int cinemaId;
    private String cinemaName;
    private String cinemaAddress;
    private String cinemaUrl;
    private String sessions;
    private String hallName;
    private String kBron;

    public SessionsDBModel(int cinemaId, String cinemaName, String cinemaAddress, String cinemaUrl, String sessions, String hallName, String kBron) {
        this.cinemaId = cinemaId;
        this.cinemaName = cinemaName;
        this.cinemaAddress = cinemaAddress;
        this.cinemaUrl = cinemaUrl;
        this.sessions = sessions;
        this.hallName = hallName;
        this.kBron = kBron;
    }

    public SessionsDBModel(){
        cinemaId = 0;
        cinemaName = "";
        cinemaAddress = "";
        cinemaUrl = "";
        sessions = "";
        hallName = "";
        kBron = "";
    }

    public int getCinemaId() {
        return cinemaId;
    }

    public void setCinemaId(int cinemaId) {
        this.cinemaId = cinemaId;
    }

    public String getCinemaName() {
        return cinemaName;
    }

    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }

    public String getCinemaAddress() {
        return cinemaAddress;
    }

    public void setCinemaAddress(String cinemaAddress) {
        this.cinemaAddress = cinemaAddress;
    }

    public String getCinemaUrl() {
        return cinemaUrl;
    }

    public void setCinemaUrl(String cinemaUrl) {
        this.cinemaUrl = cinemaUrl;
    }

    public String getSessionsFromSessions() {
        return sessions;
    }

    public void setSessionsFromSessions(String sessions) {
        this.sessions = sessions;
    }

    public String getHallName() {
        return hallName;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    public String getkBron() {
        return kBron;
    }

    public void setkBron(String kBron) {
        this.kBron = kBron;
    }

    @Override
    public String toString() {
        return "Sessions{" +
                "cinemaId=" + cinemaId +
                ", cinemaName='" + cinemaName + '\'' +
                ", cinemaAddress='" + cinemaAddress + '\'' +
                ", cinemaUrl='" + cinemaUrl + '\'' +
                ", sessions='" + sessions + '\'' +
                ", hallName='" + hallName + '\'' +
                ", kBron='" + kBron + '\'' +
                '}';
    }
}
