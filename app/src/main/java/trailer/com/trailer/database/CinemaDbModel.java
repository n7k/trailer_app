package trailer.com.trailer.database;

import io.realm.RealmObject;

public class CinemaDbModel extends RealmObject{

    private int cinemaId;
    private int cityId;
    private String cinemaName;
    private String address;
    private String phoneNumber;

    public CinemaDbModel(){
        cinemaId = 0;
        cityId = 0;
        cinemaName = "";
        address = "";
        phoneNumber = "";
    }
    public CinemaDbModel(int cinemaId, int cityId, String cinemaName, String address, String phoneNumber){
        this.cinemaId = cinemaId;
        this.cityId = cityId;
        this.cinemaName = cinemaName;
        this.address = address;
        this.phoneNumber = phoneNumber;
    }

    public int getCinemaId() {
        return cinemaId;
    }

    public void setCinemaId(int cinemaId) {
        this.cinemaId = cinemaId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getCinemaName() {
        return cinemaName;
    }

    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "CinemaDbModel{" +
                "cinemaId=" + cinemaId +
                ", cityId=" + cityId +
                ", cinemaName='" + cinemaName + '\'' +
                ", address='" + address + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
