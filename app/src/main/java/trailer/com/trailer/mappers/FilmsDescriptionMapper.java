package trailer.com.trailer.mappers;


import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import trailer.com.trailer.database.FilmsDescriptionDbModel;
import trailer.com.trailer.internet.films_description_model.FilmsDescription;

public class FilmsDescriptionMapper implements Mapper<List<FilmsDescription>,List<FilmsDescriptionDbModel>> {

    @Override
    public List<FilmsDescriptionDbModel> map(List<FilmsDescription> obj) {
        List<FilmsDescriptionDbModel> filmModelList = new ArrayList<>();
        for (FilmsDescription films : obj) {
            FilmsDescriptionDbModel model = new FilmsDescriptionDbModel();
            model.setTitle(films.getTitle());
            model.setDescription(films.getDescription());
            List<String> imageList = films.getImages();
            RealmList<String> realmListImage = new RealmList<>();
            for(String image: imageList){
                realmListImage.add(image);
            }
            model.setImages(realmListImage);
            List<String> trailersList = films.getTrailers();
            RealmList<String> realmListTrailers = new RealmList<>();
            for(String trailer: trailersList){
                realmListTrailers.add(trailer);
            }
            model.setTrailers(realmListTrailers);
            filmModelList.add(model);
        }
        return filmModelList;
    }

    @Override
    public List<FilmsDescription> reverseMap(List<FilmsDescriptionDbModel> obj) {
        List<FilmsDescription> filmsList = new ArrayList<>();
        for (FilmsDescriptionDbModel films : obj) {
            FilmsDescription model = new FilmsDescription();
            model.setTitle(films.getTitle());
            model.setDescription(films.getDescription());
        }
        return filmsList;
    }
}
