package trailer.com.trailer.mappers;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import trailer.com.trailer.database.FilmsDbModel;
import trailer.com.trailer.database.SessionsDBModel;
import trailer.com.trailer.internet.films_and_sessions_model.Films;
import trailer.com.trailer.internet.films_and_sessions_model.Sessions;

public class FilmsMapper implements Mapper<List<Films>,List<FilmsDbModel>> {

    @Override
    public List<FilmsDbModel> map(List<Films> obj) {
        List<FilmsDbModel> filmsModelList = new ArrayList<>();
        for (Films films : obj) {
            FilmsDbModel model = new FilmsDbModel();
            RealmList<SessionsDBModel> sessionCont = new RealmList<>();
            model.setId(films.getId());
            model.setName(films.getName());
            model.setImage(films.getImage());
            model.setVote(films.getVote());
            model.setCountries(films.getCountries());
            model.setActors(films.getActors());
            model.setRejisser(films.getRejisser());
            for(Sessions item : films.getSessionsList()){
                SessionsDBModel dbModel = new SessionsDBModel();
                dbModel.setCinemaId(item.getCinemaId());
                dbModel.setCinemaName(item.getCinemaName());
                dbModel.setCinemaAddress(item.getCinemaAddress());
                dbModel.setCinemaUrl(item.getCinemaUrl());
                dbModel.setSessionsFromSessions(item.getSessionsFromSessions());
                dbModel.setHallName(item.getHallName());
                dbModel.setkBron(item.getkBron());
                sessionCont.add(dbModel);
            }
            model.setSessionsList(sessionCont);
            filmsModelList.add(model);
        }
        return filmsModelList;
    }

    @Override
    public List<Films> reverseMap(List<FilmsDbModel> obj) {
        List<Films> filmsList = new ArrayList<>();
        for (FilmsDbModel films : obj) {
            Films model = new Films();
            List<Sessions> sessionsList = new ArrayList<>();
            model.setId(films.getId());
            model.setName(films.getName());
            model.setImage(films.getImage());
            model.setVote(films.getVote());
            model.setCountries(films.getCountries());
            model.setActors(films.getActors());
            model.setRejisser(films.getRejisser());
            Sessions sessions = new Sessions();
            for(SessionsDBModel item : films.getSessionsList()){
                sessions.setCinemaId(item.getCinemaId());
                sessions.setCinemaName(item.getCinemaName());
                sessions.setCinemaAddress(item.getCinemaAddress());
                sessions.setCinemaUrl(item.getCinemaUrl());
                sessions.setHallName(item.getHallName());
                sessions.setkBron(item.getkBron());
                sessionsList.add(sessions);
            }
            filmsList.add(model);
        }
        return filmsList;
    }
}
