package trailer.com.trailer.mappers;



public interface Mapper<T, E> {

    E map(T obj);
    T reverseMap(E obj);

}
